{* Mylist entry *}
{block name="frontend_index_checkout_actions_notepad"}
    <li class="navigation--entry entry--mylist" role="menuitem" aria-haspopup="true" data-drop-down-menu="true">
        <i class="fa fa-file-text" aria-hidden="true"></i> <span>{s namespace='frontend/index/checkout_actions' name='IndexLinkList'}Meine Listen{/s}</span><i class="icon--arrow-down"></i>

        {* Compare submenu *}
        <ul class="mylist--list is--rounded" role="menu">

            {* Article Compare *}
            {* Moved from topbar navigation *}
            {block name='frontend_index_navigation_inline'}
                {*{if {config name="compareShow"}}*}
                    <li class="navigation--entry entry--compare" role="menuitem" aria-haspopup="true" data-drop-down-menu="true">
                        {block name='frontend_index_navigation_compare'}
                            {action module=widgets controller=compare}
                        {/block}
                    </li>
                {*{/if}*}
            {/block}


            {* Notepad entry *}
            {block name="frontend_index_checkout_actions_notepad"}
                <li class="navigation--entry entry--notepad mylist--link" role="menuitem">
                    <a class="mylist-link" href="{url controller='note'}" title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkNotepad'}{/s}"|escape}">
                        <span class="notes--description">{s namespace='frontend/index/checkout_actions' name='IndexLinkNotepad'}{/s}</span>
                        {if $sNotesQuantity > 0}
                            <span class="badge notes--quantity">
                                {$sNotesQuantity}
                            </span>
                        {/if}
                    </a>
                </li>
            {/block}
        </ul>
    </li>
{/block}

{* My account entry *}
{block name="frontend_index_checkout_actions_my_options"}
    <li class="navigation--entry entry--account" role="menuitem">
        {block name="frontend_index_checkout_actions_account"}
            <a href="{url controller='account'}" title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}"|escape}" class="entry--link account--link">
                <i class="fa fa-user" aria-hidden="true"></i>
                <span class="account--display">
                    {s namespace='frontend/index/checkout_actions' name='IndexLinkAccount'}{/s}
                </span>
            </a>
        {/block}
    </li>
{/block}

{* Cart entry *}
{block name="frontend_index_checkout_actions_cart"}
    <li class="navigation--entry entry--cart" role="menuitem">
        <i class="icon--basket"></i>
        <span class="badge is--minimal cart--quantity{if $sBasketQuantity < 1} is--hidden{/if}">{$sBasketQuantity}</span>
        <a class="cart--link" href="{url controller='checkout' action='cart'}" title="{"{s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}"|escape}">
            <span class="cart--display">
                {if $sUserLoggedIn}
                    {s name='IndexLinkCheckout' namespace='frontend/index/checkout_actions'}{/s}
                {else}
                    {s namespace='frontend/index/checkout_actions' name='IndexLinkCart'}{/s}
                {/if}
            </span>
            <span class="cart--amount">
                {$sBasketAmount|currency} {s name="Star" namespace="frontend/listing/box_article"}{/s}
            </span>
        </a>
        <div class="ajax-loader">&nbsp;</div>
    </li>
{/block}

{block name="frontend_index_checkout_actions_inner"}{/block}
